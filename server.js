var express       = require('express')
  , app           = express()
  , bodyParser    = require('body-parser')
  , morgan        = require('morgan')
  , mongoose      = require('mongoose')
  , engine        = require('ejs-locals')
  , path          = require('path')
  , configDB      = require('./express/config/database.js')
  , port          = process.env.PORT || 8080;

// MONGOOSE
mongoose.connect(configDB.contactsApp);

// CONFIGURE & START THE SERVER
app
  .use(morgan('dev'))                                         // log every request to the console
  .use(bodyParser.urlencoded({ extended: true }))             // get information from html forms
  .use(bodyParser.json())
  
  .set('views', path.join(__dirname, 'public'))               // view engine setup
  .engine('html', engine)
  .set('view engine', 'html')
    
  .use(express.static(__dirname + '/public'))                 // static & routes
  .use('/api/V1', require('./express/routes/api').apiRouter)
  .use('/otherapp/', function (req, res){
    res.render('otherapp');                                   // render the Single Page App
  })
  .use('/contactsapp/', function (req, res){
    res.render('contactsapp');                                // render the Single Page App
  })
  .use('/', function (req, res){
    res.render('contactsapp');                                // render the Single Page App
  })
  .listen(port);

console.log("ContactsApp is waiting on " + port);
