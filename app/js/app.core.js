(function() {
  'use strict';

  var app = angular
    .module('ContactsApp', ['ui.bootstrap', 'ui.router', 'satellizer'])
    .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$authProvider', require('js/app.router')])

    // Factories
    .factory('Contacts',  ['$http', require('js/contacts/contacts.service')])
    .factory('Logs',      ['$http', require('js/logs/logs.service')])
    
    // Controllers
    .controller('ErrorModalController',     ['$modalInstance', 'errorData', require('js/error.modal.controller')])
    .controller('ContactsModalController',  ['$modal', '$modalInstance', 'modalData', 'Contacts', require('js/contacts/contacts.modal.controller')])
    .controller('ContactsPageController',   ['$modal', 'Contacts', require('js/contacts/contacts.page.controller')])
    .controller('LogsController',           ['Logs', require('js/logs/logs.controller')])

    .run(function ($rootScope) {
      $rootScope.$on('$stateChangeStart', function() {
        document.body.scrollTop = document.documentElement.scrollTop = 0; // Scroll to the top when state changes
        if($rootScope.isCollapsed)
          $rootScope.isCollapsed = !$rootScope.isCollapsed;               // Close responsive navbar when state changes
      });
    });
})();
