  (function() {
    'use strict';
    module.exports = function ($http, $window, $auth, $state) {
      var vm    = this;
      vm.login  = {};
      vm.data   = [];
      vm.test   = test;
      vm.reset  = reset;

      function reset() {
        vm.data = [];
      }

      function test() {
        if($auth.isAuthenticated()) {         
          $http.get('api/users').success(function(users) {
            vm.data = users;
          }).error(function(error) {
            vm.data = error;
          });
        }
        else {
          console.log($auth.getToken())

          $http({
            method: 'GET',
            url: 'api/users',
            params: {token: $auth.getToken()}
          })
          .then(
            function successCallback(error) {
              vm.data = error;
            },
            function errorCallback(error) {
              vm.data = error;
            }
          );
          //$state.go('login', {});
        }
      }
    };
  })();
  