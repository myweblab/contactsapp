(function() {
  'use strict';

  module.exports = function ($http) {
    var url = '/api/V1/contacts';
    return {
      get     : function() {
        return $http.get(url);
      },
      getOne  : function(id) {
        return $http.get(url+'/'+id);
      },
      create  : function(data) {
        return $http.post(url, data);
      },
      edit    : function(id, data) {
        return $http.put(url+'/'+id, data);
      },
      delete  : function(id) {
        return $http.delete(url+'/'+id);
      }
    }
  };
})();
