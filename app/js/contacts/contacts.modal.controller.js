(function() {
  'use strict';

  module.exports = function ($uibModal, $modalInstance, modalData, Contacts) {
    var vm                = this;
    vm.modalInfos   = modalData;
    vm.disabledMe   = (modalData.action == 'delete');
    vm.model        = {};
    vm.ok           = ok;
    vm.cancel       = cancel;
    vm.errorModal   = errorModal;
    init();

    function init() {
      vm.loading = true;
      if(modalData.id) {
        Contacts
          .getOne(modalData.id)
          .success(function(data) {
            vm.model = data;
          });
      }
      vm.loading = false;
    }

    function cancel() {
      $modalInstance.dismiss('cancel');
    }

    function errorModal(data) {
      var modalInstance = $uibModal.open({
        templateUrl: 'errorModal.html',
        controller:  'ErrorModalController as vm',
        resolve: {
          errorData: function() { return data; }
        }
      })
      .result.then(function (mess) {
        console.log(mess);
      });
    }

    function ok() {
      switch(modalData.action) {
        case 'create':
          Contacts
            .create(vm.model)
            .success(function(data) {
              $modalInstance.close('Contact created: ' + data._id);
            })
            .error(function(data) {
              vm.errorModal(data);
            });
          break;
        case 'edit':
          Contacts
            .edit(modalData.id, vm.model)
            .success(function(data) {
              $modalInstance.close('Contact updated: ' + data._id);
            })
            .error(function(data) {
              vm.errorModal(data);
            });
          break;
        case 'delete':
          Contacts
            .delete(modalData.id)
            .success(function(data) {
              $modalInstance.close('Contact deleted: ' + modalData.id);
            })
            .error(function(data) {
              vm.errorModal(data);
            });
          break;
      }
    }
  };
})();
