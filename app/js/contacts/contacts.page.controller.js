(function() {
  'use strict';

  module.exports = function ($uibModal, Contacts) {
    var vm                = this;
    vm.getContacts        = getContacts;
    vm.openContactsModal  = openContactsModal;
    getContacts();

    function getContacts() {
      vm.loadingContacts = true;
      Contacts
        .get()
        .success(function(data) {
          vm.contacts        = data;
          vm.loadingContacts = false;
        });
    }

    function openContactsModal(action, id) {  
      var modalData               = {};
      modalData.id                = id;
      modalData.action            = action;
      switch(action) {
        case 'create':
          modalData.titel         = 'New contact';
          modalData.buttonClass   = 'btn-success';
          modalData.submitButton  = 'Create';
          break;
        case 'edit':
          modalData.titel         = 'Edit contact';
          modalData.buttonClass   = 'btn-warning';
          modalData.submitButton  = 'Edit';
          break;
        case 'delete':
          modalData.titel         = 'Delete contact';
          modalData.buttonClass   = 'btn-danger';
          modalData.submitButton  = 'Delete';
          break;
      }
      var modalInstance = $uibModal.open({
        templateUrl: 'contactsModal.html',
        controller:  'ContactsModalController as vm',
        resolve: {
          modalData: function() { return modalData; }
        }
      })
      .result.then(function (mess) {
        console.log(mess);
        getContacts();
      });
    }
  }
})();