  (function() {
    'use strict';
    module.exports = function ($modalInstance, errorData) {
      var vm    = this;
      vm.error  = errorData;
      vm.ok     = function () {
        $modalInstance.close('Error closed.');
      };
    };
  })();
