(function() {
  'use strict';

  module.exports = function (Logs) {
    var vm            = this;
    vm.sortType       = 'date';            // set the default sort type
    vm.sortReverse    = true;              // set the default sort order
    vm.searchInputs   = false;

    vm.getLogs = getLogs;
    getLogs();

    function getLogs() {
      vm.loading = true;
      Logs
        .get()
        .success(function(data) {
          vm.logs     = data;
          vm.loading  = false;
        });
    }
  };
})();
