(function() {
  'use strict';

  module.exports = function ($http) {
    var url = '/api/V1/logs';
    return {
      get     : function() {
        return $http.get(url);
      },
      getOne  : function(id) {
        return $http.get(url+'/'+id);
      }
    }
  };
})();
