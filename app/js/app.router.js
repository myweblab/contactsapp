  (function() {
    'use strict';
    module.exports = function ($stateProvider, $urlRouterProvider, $locationProvider, $authProvider) {
      // Satellizer configuration that specifies which API route the JWT should be retrieved from
      $authProvider.loginUrl  = '/api/authenticate';
      $authProvider.authToken = '';

      $urlRouterProvider.otherwise('404');
      $stateProvider
        .state('home', {
          url: '/',
          templateUrl: '../partials/home.html'
        })
        .state('contacts', {
          url: '/contacts',
          templateUrl: '../partials/contacts.html'
        })
        .state('logs', {
          url: '/logs',
          templateUrl: '../partials/logs.html'
        })
        .state('profile', {
          url: '/profile',
          templateUrl: '../partials/profile.html'
        })
        .state('404', {
          url: '/404',
          templateUrl: '../partials/404.html'
        });

      $locationProvider.html5Mode(true); // use the HTML5 History API
    }
  })();