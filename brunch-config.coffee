exports.config =
  files:
    javascripts:
      joinTo:
        'app.js': /^app/
        'vendor.js': /^vendor/
      order:
        before: [
          'vendor/scripts/jquery-2.1.4.js',
          'vendor/scripts/underscore-1.8.3.js',
          'vendor/scripts/angular-1.5.0.js',
          'vendor/scripts/ui-bootstrap-0.14.1.js',
          'vendor/scripts/ui-bootstrap-tpls-0.14.3.js',
          'vendor/scripts/angular-ui-router-0.2.10.js',
          'vendor/scripts/angular-locale_de-de.js',
          'vendor/scripts/satellizer-0.11.3.js',

          # Twitter Bootstrap jquery plugins
          'vendor/scripts/bootstrap/affix.js',
          'vendor/scripts/bootstrap/alert.js',
          'vendor/scripts/bootstrap/button.js',
          'vendor/scripts/bootstrap/carousel.js',
          'vendor/scripts/bootstrap/collapse.js',
          'vendor/scripts/bootstrap/dropdown.js',
          'vendor/scripts/bootstrap/modal.js',
          'vendor/scripts/bootstrap/tooltip.js',
          'vendor/scripts/bootstrap/popover.js',
          'vendor/scripts/bootstrap/scrollspy.js',
          'vendor/scripts/bootstrap/tab.js',
          'vendor/scripts/bootstrap/transition.js'
        ]

    stylesheets:
      joinTo:
        'app.css': /^app/
        'vendor.css': /^vendor/

  plugins:
    autoReload:
      enabled:
        css: on
        js: on
        assets: on
    sass:
      precision: 8
