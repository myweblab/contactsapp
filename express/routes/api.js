var express   = require('express')
  , router    = express.Router()
  , User      = require('./../models/user')
  , Log       = require('./../models/log')
  , Contact   = require('./../models/contact')
  , secret    = require('./../config/database.js').secret
  , jwt       = require('jsonwebtoken');


var checkQueryParams = function(skip, limit, params){
  var query = {}, skipLimit = {};
  for (var prop in params){
    if(params[prop]) {
      if(!isNaN(parseFloat(params[prop])) && isFinite(params[prop]))
        query[prop] = params[prop];
      else if(typeof params[prop] == "string")
        query[prop] = new RegExp(params[prop], 'i');
    }
  }
  skipLimit.s = Number(skip)  || 0;
  skipLimit.l = Number(limit) || 150;
  return [query, skipLimit];
};


function saveLog(cat, w) {
  var log = new Log({ categorie: cat, what: w }).save(function(err){
    if(err) console.log(err);
  });
}


// ====================================================
//            /logs    ===========================
router.route('/logs')
.get(function(req, res) {
  Log.find(function(err, result) {
    if(err) return res.send(err);
    res.status(200).json(result);
  });
});


// ====================================================
//            /contacts    ===========================
router.route('/contacts')
.get(function(req, res) {
  var query = checkQueryParams(req.query.skip, req.query.limit, {
    firstname  : req.query.firstname,
    lastname   : req.query.lastname,
    email      : req.query.email,
    phone      : req.query.phone
  });
  Contact.find(query[0]).skip(query[1].s).limit(query[1].l).exec(function(err, result) {
    if(err) return res.send(err);
    res.status(200).json(result);
  });
})
.post(function(req, res) {
  var obj = new Contact({
    firstname  : req.body.firstname,
    lastname   : req.body.lastname,
    email      : req.body.email,
    phone      : req.body.phone
  });
  obj.save(function(err) {
    if(err) return res.status(400).send(err);
    saveLog('Contact', 'Contact '+obj.firstname+' '+obj.lastname+' created');
    res.status(201).json(obj);
  });
})
.all(function(req, res) {
  res.status(404).json({'type':'error', 'msg':'not implemented'});
});

router.route('/contacts/:obj_id')
.get(function(req, res) {
  Contact.findById(req.params.obj_id).exec(function(err, obj) {
    if(err) return res.status(400).send(err);
    res.json(obj);
  });
})
.put(function(req, res) {
  Contact.findById(req.params.obj_id, function(err, obj) {
    if(err) return res.status(400).send(err);

    if(req.body.firstname)  obj.firstname = req.body.firstname;
    if(req.body.lastname)   obj.lastname  = req.body.lastname;
    if(req.body.email)      obj.email     = req.body.email;
    if(req.body.phone)      obj.phone     = req.body.phone;
    
    obj.save(function(err) {
      if(err) return res.status(400).send(err);
      saveLog('Contact', 'Contact '+obj.firstname+' '+obj.lastname+' updated');
      res.status(200).json(obj);
    });
  });
})
.delete(function(req, res) {
  Contact.findByIdAndRemove(req.params.obj_id).exec(function(err, obj) {
    if(err) return res.status(400).send(err);
    saveLog('Contact', 'Contact '+obj.firstname+' '+obj.lastname+' deleted');
    res.status(204).json({'type': 'success', 'msg': 'Contact '+obj.firstname+' '+obj.lastname+' deleted'});
  });
});

exports.apiRouter = router;
