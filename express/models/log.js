var mongoose  = require('mongoose')
    Schema    = mongoose.Schema
  , logSchema = Schema({
  date        : { type: Date, default: Date.now, required: true },
  categorie   : { type: String, required: true },
  //who         : { type: Schema.Types.ObjectId, ref: 'User', required: true },
  what        : { type: String, required: true }
});

module.exports = mongoose.model('Log', logSchema);