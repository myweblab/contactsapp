var mongoose        = require('mongoose')
  , uniqueValidator = require('mongoose-unique-validator')
  , bcrypt          = require('bcrypt-nodejs')
  , Schema          = mongoose.Schema 
  , contactSchema   = Schema({
  firstname       : { type: String, required: true },
  lastname        : { type: String, required: true },
  email           : { type: String, required: false, unique: true },
  phone           : { type: String, required: false },
  birthDay        : { type: String, required: false }
})
.plugin(uniqueValidator, { message: 'Error, expected {PATH} to be unique.' });

module.exports = mongoose.model('Contact', contactSchema);
