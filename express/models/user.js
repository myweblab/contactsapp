var mongoose        = require('mongoose')
  , uniqueValidator = require('mongoose-unique-validator')
  , bcrypt          = require('bcrypt-nodejs')
  , Schema          = mongoose.Schema 
  , userSchema      = Schema({
  firstname       : { type: String, required: true },
  lastname        : { type: String, required: true },
  local           : {
    email         : { type: String, required: true, unique: true },
    password      : { type: String, required: true }
  },
  admin           : {
    loginApp      : {
      active      : Boolean,
      superadmin  : Boolean
    }
  },
  user            : {
    active        : Boolean
  },
  birthDay        : { type: String, required: false }
})
.plugin(uniqueValidator, { message: 'Error, expected {PATH} to be unique.' });


userSchema.methods.generateHash = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};
userSchema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.local.password);
};

module.exports = mongoose.model('User', userSchema);
